# Create an ECS cluster
resource "aws_ecs_cluster" "ecs_cluster" {
  name = "infralab-ecs-cluster"
}

# resource "aws_ecs_capacity_provider" "ecs_capacity_provider" {
#   name = "test-cp1"

#   auto_scaling_group_provider {
#     auto_scaling_group_arn = aws_autoscaling_group.ecs_asg.arn

#     managed_scaling {
#       maximum_scaling_step_size = 1000
#       minimum_scaling_step_size = 1
#       status                    = "ENABLED"
#       target_capacity           = 3
#     }
#   }
# }

# resource "aws_ecs_cluster_capacity_providers" "example" {
#   cluster_name = aws_ecs_cluster.ecs_cluster.name

#   capacity_providers = [aws_ecs_capacity_provider.ecs_capacity_provider.name]

#   default_capacity_provider_strategy {
#     base              = 1
#     weight            = 100
#     capacity_provider = aws_ecs_capacity_provider.ecs_capacity_provider.name
#   }
# }


data "template_file" "app" {
  template = file("templates/env_app.json.tpl")

  # vars = {
  #   image  = var.docker_image_url
  #   region = var.region
  # }
}

########################################################################################################################
## Define the ECS task definition for the service for app
########################################################################################################################
# 
resource "aws_ecs_task_definition" "ecs_task_definition" {
  family             = "infralab-ecs-task"
  network_mode       = "awsvpc"
  execution_role_arn = "arn:aws:iam::730866387406:role/ecsTaskExecutionRole"
  cpu                = 256
  memory             = 512
  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }
  requires_compatibilities = ["EC2", "FARGATE"]
  container_definitions = data.template_file.app.rendered
  #  jsonencode([
  #   {
  #     name      = "comentarios-api"
  #     image     = "registry.gitlab.com/guilhermefpv/comentarios-api/main:latest"
  #     cpu       = 256
  #     memory    = 512
  #     essential = true
  #     portMappings = [
  #       {
  #         containerPort = 8000
  #         hostPort      = 8000
  #         protocol      = "tcp"
  #       }
  #     ]
  #     environment = data.template_file.app.rendered

  #   }
  # ])
}


########################################################################################################################
## Create an ECS service APP
########################################################################################################################

resource "aws_ecs_service" "ecs_service" {
  name            = "infralab-ecs-service"
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.ecs_task_definition.arn
  desired_count   = 1

  network_configuration {
    subnets         = [aws_subnet.subnet.id, aws_subnet.subnet2.id]
    security_groups = [aws_security_group.security_group.id]
  }

  force_new_deployment = true
  placement_constraints {
    type = "distinctInstance"
  }

  triggers = {
    # redeployment = timestamp()
    redeployment = plantimestamp()
  }

  # capacity_provider_strategy {
  #   capacity_provider = aws_ecs_capacity_provider.ecs_capacity_provider.name
  #   weight            = 100
  # }

  load_balancer {
    target_group_arn = aws_lb_target_group.ecs_tg.arn
    container_name   = "comentarios-api"
    container_port   = 8000
  }

  # depends_on = [aws_autoscaling_group.ecs_asg]
  depends_on = [aws_autoscaling_group.ecs_autoscaling_group]  
}


########################################################################################################################
## Create an ECS task definition Grafana
########################################################################################################################

resource "aws_ecs_task_definition" "ecs_task_definition_grafana" {
  family             = "infralab-ecs-task"
  network_mode       = "awsvpc"
  execution_role_arn = "arn:aws:iam::730866387406:role/ecsTaskExecutionRole"
  cpu                = 256
  memory             = 512
  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }
  requires_compatibilities = ["EC2", "FARGATE"]
  container_definitions = jsonencode([
    {
      name      = "grafana"
      image     = "grafana/grafana:latest"
      cpu       = 256
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 3000
          protocol      = "tcp"
        },
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          "awslogs-create-group": "true",
          "awslogs-group" = "awslogs-grafana"
          "awslogs-region" = "us-east-1"
          "awslogs-stream-prefix": "awslogs-grafana"
          "mode": "non-blocking", 
          "max-buffer-size": "25m"
        }
      }
    },
  ])
}


########################################################################################################################
## Create an ECS service Grafana
########################################################################################################################

resource "aws_ecs_service" "grafana" {
  name            = "grafana-service"
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.ecs_task_definition_grafana.arn
  desired_count   = 1
  # launch_type     = "FARGATE"

  network_configuration {
    subnets         = [aws_subnet.subnet.id, aws_subnet.subnet2.id]
    security_groups = [aws_security_group.security_group.id]
    #assign_public_ip = true
  }

  force_new_deployment = true
  placement_constraints {
    type = "distinctInstance"
  }

  triggers = {
    # redeployment = timestamp()
    redeployment = plantimestamp()
  }

  # capacity_provider_strategy {
  #   capacity_provider = aws_ecs_capacity_provider.ecs_capacity_provider.name
  #   weight            = 100
  # }

  load_balancer {
    target_group_arn = aws_lb_target_group.ecs_tg.arn
    container_name   = "grafana"
    container_port   = 3000
  }

  depends_on = [aws_autoscaling_group.ecs_autoscaling_group]
}