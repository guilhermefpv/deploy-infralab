# resource "aws_launch_template" "ecs_launch_template" {
#   name                   = "ecs_launch_template"
#   #name_prefix           = "ecs-infralab"
#   image_id               = "ami-0c7217cdde317cfec"
#   instance_type          = "t2.micro"
#   user_data              = base64encode(data.template_file.user_data.rendered)
#   key_name               = "ec2-esc"
#   vpc_security_group_ids = [aws_security_group.security_group.id]
#   # iam_instance_profile {
#   #   name = "ecsInstanceRole"
#   # }

#   iam_instance_profile {
#     name = "lab_ecs_instance_profile"
#   }

#   monitoring {
#     enabled = true
#   }

#   block_device_mappings {
#     device_name = "/dev/xvda"
#     ebs {
#       volume_size = 20
#       volume_type = "gp2"
#     }
#   }

#   tag_specifications {
#     resource_type = "instance"
#     tags = {
#       Name = "ecs-instance"
#     }
#   }

#   # user_data = filebase64("${path.module}/user_data.sh")
# }


# data "template_file" "user_data" {
#   template = file("user_data.sh")

#   vars = {
#     ecs_cluster_name = aws_ecs_cluster.ecs_cluster.name
#   } "default
# }


# resource "aws_autoscaling_group" "ecs_asg" {
#   name = "infralab-ecs-asg"
#   vpc_zone_identifier = [aws_subnet.subnet.id, aws_subnet.subnet2.id]
#   desired_capacity    = 1
#   max_size            = 2
#   min_size            = 1

#   launch_template {
#     id      = aws_launch_template.ecs_launch_template.id
#     version = "$Latest"
#   }

#   tag {
#     key                 = "AmazonECSManaged"
#     value               = true
#     propagate_at_launch = true
#   }
# }

########################################################################################################################
## Creates an ASG linked with our main VPC
########################################################################################################################

resource "aws_autoscaling_group" "ecs_autoscaling_group" {
  name                  = "${var.namespace}_ASG_${var.environment}"
  max_size              = var.autoscaling_max_size
  min_size              = var.autoscaling_min_size
  # vpc_zone_identifier   = aws_subnet.private.*.id
  vpc_zone_identifier = [aws_subnet.subnet.id, aws_subnet.subnet2.id]
  health_check_type     = "EC2"
  protect_from_scale_in = true

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances"
  ]

  launch_template {
    id      = aws_launch_template.ecs_launch_template.id
    version = "$Latest"
  }

  instance_refresh {
    strategy = "Rolling"
  }

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "${var.namespace}_ASG_${var.environment}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Scenario"
    propagate_at_launch = false
    value               = var.scenario
  }
}

//Alb
resource "aws_lb" "ecs_alb" {
  name               = "ecs-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.security_group.id]
  subnets            = [aws_subnet.subnet.id, aws_subnet.subnet2.id]

  tags = {
    Name = "ecs-alb"
  }
}

resource "aws_lb_listener" "ecs_alb_listener" {
  load_balancer_arn = aws_lb.ecs_alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ecs_tg.arn
  }
}

resource "aws_lb_target_group" "ecs_tg" {
  name        = "ecs-target-group"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.main.id

  health_check {
    path = "/"
  }
}
