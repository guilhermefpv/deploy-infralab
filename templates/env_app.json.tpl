[
    {
        "name": "comentarios-api",
        "image": "registry.gitlab.com/guilhermefpv/flask-app/main:latest",
        "cpu": 256,
        "memory": 512,
        "essential": true,
        "portMappings": [
            {
                "containerPort": 8000,
                "hostPort": 8000,
                "protocol": "tcp"
            }
        ],
        "environment": [
                {
                    "name": "FLASK_DEBUG",
                    "value": "1"
                },
                {
                    "name": "GUNICORN_WORKERS",
                    "value": "4"
                },
                {
                    "name": "FLASK_ENV",
                    "value": "production"
                },
                {
                    "name": "LOG_LEVEL",
                    "value": "debug"
                },
                {
                    "name": "ENV_LOG_LEVEL",
                    "value": "debug"
                }
        ]
    }
]
