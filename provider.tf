terraform {
  cloud {
    organization = "GuilhermeFPV"
    
    workspaces {
      name = "deploy-infralab"
    }
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.37.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~> 3.5.1"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0.4"
    }

    cloudinit = {
      source  = "hashicorp/cloudinit"
      version = "~> 2.3.2"
    }

    docker = {
      source  = "kreuzwerker/docker"
      version = "~>2.20.0"
    }
  }
}

provider "docker" {}

provider "aws" {
  region = var.aws_region
}

